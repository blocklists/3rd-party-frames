This is a list of static filtering rules, to allo load different 3rd party frames using click2play.

First, use a Dynamic filtering rule to block all 3rd party frames: `* * 3p-frame block`

This is important to be able to disable the rule in the dashboard if a page is not working properly.
